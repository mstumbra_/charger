#[derive(Copy,Clone)]
enum DataType {
    Command = 0,
    Symbol  = 1,
}

enum Command {
    ClearDisplay            = 0x01,
    ReturnHome              = 0x02,
    EntryModeSet            = 0x04,
    DisplayOnOffControl     = 0x08,
    CursorOrDisplayShift    = 0x10,
    FunctionSet             = 0x20,
    SetCgramAddress         = 0x40,
    SetDdramAddress         = 0x80,
}

mod bus_flags
{
    pub const OE            : u8 = 4;
    pub const BACKLIGHT     : u8 = 8;
}

mod entry_mode_flags
{
    pub const SHIFT         : u8 = 1;
    pub const INCREMENT     : u8 = 2;
}

mod on_off_ctrl_flags
{
    pub const BLINKING_ON   : u8 = 1;
    pub const CURSOR_ON     : u8 = 2;
    pub const DISPLAY_ON    : u8 = 4;
}

mod shift_flags
{
    pub const RIGHT         : u8 = 4;
    pub const DISPLAY       : u8 = 8;
}

mod function_flags
{
    pub const BUS_8_BIT     : u8 = 0x10;
    pub const TWO_LINES     : u8 = 0x08;
    // pub const FONT_5_10     : u8 = 0x04;
}

use embedded_hal::blocking::i2c::Write;
use embedded_hal::blocking::delay::DelayUs;

// type Result<T> = core::result::Result<(), T>;

pub struct Lcd<T: Write>
{
    bus: T,
    address: u8,
}

type Result<T> = core::result::Result<(), LcdError<T>>;

pub enum LcdError<T: Write>
{
    ParamError,
    BusError(T::Error),
}

impl<T:Write> LcdError<T>
{
    fn from(err: T::Error) -> Self
    {
        Self::BusError(err)
    }
}

// impl<T:Write> From<T::Error> for MyError<T>
// {
//     fn from(err: T::Error) -> Self
//     {
//         Self::BusError(err)
//     }
// }

impl<T: Write> Lcd<T>
{

    pub fn new(bus: T, address: u8) -> Self
    {
        Lcd {bus, address}
    }

    pub fn clear<Delay: DelayUs<u16>>(&mut self, delay: &mut Delay) -> Result<T>
    {
        self.command(Command::ClearDisplay as u8)?;
        delay.delay_us(1500);
        Ok(())
    }

    pub fn carriage_return(&mut self) -> Result<T>
    {
        self.command(Command::ReturnHome as u8)
    }

    pub fn set_entry_mode(&mut self, increment: bool, display_shift: bool) -> Result<T>
    {
        self.command(Command::EntryModeSet as u8 |
                    if increment     { entry_mode_flags::INCREMENT } else { 0 } |
                    if display_shift { entry_mode_flags::SHIFT     } else { 0 } )
    }

    pub fn on_control(&mut self, display: bool, cursor: bool, blinking: bool) -> Result<T>
    {
        self.command(Command::DisplayOnOffControl as u8 |
                    if display  { on_off_ctrl_flags::DISPLAY_ON } else { 0 } |
                    if cursor   { on_off_ctrl_flags::CURSOR_ON  } else { 0 } |
                    if blinking { on_off_ctrl_flags::BLINKING_ON} else { 0 } )
    }

    pub fn cursor_or_display_shift(&mut self, display: bool, right: bool) -> Result<T>
    {
        self.command(Command::CursorOrDisplayShift as u8 |
                    if display { shift_flags::DISPLAY } else { 0 } |
                    if right   { shift_flags::RIGHT   } else { 0 } )
    }

    pub fn set_pos(&mut self, pos: u8) -> Result<T>
    {
        self.command(Command::SetDdramAddress as u8 | pos)
    }

    pub fn set_custom_symbol(&mut self, id: u8, data: &[u8; 8]) -> Result<T>
    {
        self.set_character_gen_address(id * 8)?;
        self.write(data)
    }

    pub fn init<Delay: DelayUs<u16>>(&mut self, delay: &mut Delay) -> Result<T>
    {
        self.set_display_function(function_flags::TWO_LINES | function_flags::BUS_8_BIT)?;
        self.write_4bit(0x20)?;
        self.set_display_function(function_flags::TWO_LINES)?;
        self.on_control(true, false, false)?;
        self.set_entry_mode(true, false)?;
        self.clear(delay)
    }

    pub fn write(&mut self, str : &[u8]) -> Result<T>
    {
        for symbol in str
        {
            self.write_8bit(*symbol, DataType::Symbol)?;
        }
        Ok(())
    }

    fn set_display_function(&mut self, function_flags: u8) -> Result<T>
    {
        self.command(Command::FunctionSet as u8 | function_flags)
    }

    fn set_character_gen_address(&mut self, address: u8) -> Result<T>
    {
        self.command(Command::SetCgramAddress as u8 | address)
    }

    fn command(&mut self, command: u8) -> Result<T>
    {
        self.write_8bit(command, DataType::Command)
    }

    fn write_8bit(&mut self, byte: u8, data_type: DataType) -> Result<T>
    {
        let data = [
            (byte & 0xF0) | bus_flags::BACKLIGHT |                 data_type as u8,
            (byte & 0xF0) | bus_flags::BACKLIGHT | bus_flags::OE | data_type as u8,
            (byte & 0xF0) | bus_flags::BACKLIGHT |                 data_type as u8,
            (byte << 4)   | bus_flags::BACKLIGHT |                 data_type as u8,
            (byte << 4)   | bus_flags::BACKLIGHT | bus_flags::OE | data_type as u8,
            (byte << 4)   | bus_flags::BACKLIGHT |                 data_type as u8,
        ];
        match self.bus.write(self.address, &data)
        {
            Ok(_) => Ok(()),
            Err(e) => Err(LcdError::from(e)),
        }
    }

    fn write_4bit(&mut self, nibble: u8) -> Result<T>
    {
        let data = [
            nibble | bus_flags::BACKLIGHT,
            nibble | bus_flags::BACKLIGHT | bus_flags::OE,
            nibble | bus_flags::BACKLIGHT,
        ];
        match self.bus.write(self.address, &data)
        {
            Ok(_) => Ok(()),
            Err(e) => Err(LcdError::from(e)),
        }
    }
}
