#![no_std]
#![no_main]

pub mod lcd;
use core::cmp::Ordering;

use panic_halt as _;
use stm32f3xx_hal as hal;
use cortex_m_rt::entry;
use hal::prelude::*;

const LCD_I2C_ADDRESS: u8 = 0x27;

#[entry]
fn main() -> ! {
    let cp = cortex_m::Peripherals::take().unwrap();
    let mut dp = hal::pac::Peripherals::take().unwrap();

    let mut flash = dp.FLASH.constrain();
    let mut rcc = dp.RCC.constrain();

    let clocks = rcc.cfgr
    .use_hse(8.mhz())
    .bypass_hse()
    .sysclk(72.mhz())
    .hclk(72.mhz())
    .pclk1(36.mhz())
    .pclk2(72.mhz())
    .freeze(&mut flash.acr);
    let mut gpioa = dp.GPIOA.split(&mut rcc.ahb);
    let mut gpiob = dp.GPIOB.split(&mut rcc.ahb);
    let mut gpioc = dp.GPIOC.split(&mut rcc.ahb);

    let button = gpioc.pc13.into_floating_input(&mut gpioc.moder, &mut gpioc.pupdr);
    let mut delay = hal::delay::Delay::new(cp.SYST, clocks);
    let mut pwm = {
        // Enable 2x multiplyier for TIM1
        let rcc_regs = unsafe { &*hal::pac::RCC::ptr() };
        rcc_regs.cfgr3.write(|w| w.tim1sw().set_bit());
        let gpioc_regs = unsafe{ &*hal::pac::GPIOC::ptr() };
        gpioc_regs.ospeedr.write(|w| w.ospeedr2().very_high_speed());
        let gpiob_regs = unsafe{ &*hal::pac::GPIOB::ptr() };
        gpiob_regs.ospeedr.write(|w| w.ospeedr1().very_high_speed());

        let pc2 = gpioc.pc2.into_af2(&mut gpioc.moder, &mut gpioc.afrl);
        let pb1 = gpiob.pb1.into_af6(&mut gpiob.moder, &mut gpiob.afrl);
        let tim_ch3 = hal::pwm::tim1(dp.TIM1, 144, 1.mhz().into(), &clocks).2
        .output_to_pc2(pc2).output_to_pb1(pb1);
        let tim_regs = unsafe{ &*hal::pac::TIM1::ptr() };
        // tim_regs.bdtr.write(unsafe { |w| w.dtg().bits(0xFF) });
        tim_regs.ccer.modify(|_, w| w.cc3e().set_bit());
        tim_ch3
    };

    let mut lcd = {
        let scl = gpioa.pa9.into_af4(&mut gpioa.moder, &mut gpioa.afrh);
        let sda = gpioa.pa10.into_af4(&mut gpioa.moder, &mut gpioa.afrh);
        let i2c = hal::i2c::I2c::new(dp.I2C2, (scl,sda), 100.khz(), clocks, &mut rcc.apb1);
        lcd::Lcd::new(i2c, LCD_I2C_ADDRESS)
    };

    let serial = {
        let tx = gpioa.pa2.into_af7(&mut gpioa.moder, &mut gpioa.afrl);
        let rx = gpioa.pa3.into_af7(&mut gpioa.moder, &mut gpioa.afrl);
        hal::serial::Serial::usart2(dp.USART2, (tx, rx), 1_500_000.bps(), clocks, &mut rcc.apb1)
    };
    // let results : [nb::Result<(), core::convert::Infallible>; "Hello world".len()];
    // let iter = results.into_iter();
    let (mut uart_tx, _) = serial.split();

    lcd.init(&mut delay).ok();
    lcd.write(b"Calibrating ADC").ok();
    lcd.set_pos(0x40).ok();
    lcd.write(b"Please Standby...").ok();

    // set up adc1
    let mut adc1 = hal::adc::Adc::adc1(dp.ADC1, &mut dp.ADC1_2,
        &mut rcc.ahb, stm32f3xx_hal::adc::CkMode::default(), clocks,);
    let mut adc2 = hal::adc::Adc::adc2(dp.ADC2, &mut dp.ADC1_2,
        &mut rcc.ahb, stm32f3xx_hal::adc::CkMode::default(), clocks,);
    let mut adc3 = hal::adc::Adc::adc3(dp.ADC3, &mut dp.ADC3_4,
            &mut rcc.ahb, stm32f3xx_hal::adc::CkMode::default(), clocks,);
    let mut vbat_pin = gpioa.pa0.into_analog(&mut gpioa.moder, &mut gpioa.pupdr);
    let mut vin_pin = gpioa.pa1.into_analog(&mut gpioa.moder, &mut gpioa.pupdr);
    let mut current_pin = gpioa.pa4.into_analog(&mut gpioa.moder, &mut gpioa.pupdr);
    let mut v2_5_pin = gpiob.pb0.into_analog(&mut gpiob.moder, &mut gpiob.pupdr);

    lcd.clear(&mut delay).ok();
    lcd.write(b"Vin: 00,00V").ok();
    lcd.set_pos(0x40).ok();
    lcd.write(b"U:00,00V;I:0,00A").ok();

    pwm.enable();
    pwm.set_duty(10);

    loop {
        let adc_v_in : u16 = adc1.read(&mut vin_pin).unwrap();
        let adc_v_bat: u16 = adc1.read(&mut vbat_pin).unwrap();
        let adc_v2_5 : u16 = adc3.read(&mut v2_5_pin).unwrap();
        let adc_i_bat: u16 = adc2.read(&mut current_pin).unwrap();
        const HEADER: u16 = 0xFFFF;
        for data in [HEADER, adc_v_in, adc_v_bat, adc_v2_5, adc_i_bat].iter()
        {
            for byte in data.to_le_bytes().iter()
            {
                while uart_tx.write(*byte).is_err() {}
            }
        }

        let v_in = (adc_v_in as f32 * 3.3 * 6.6  * 100.0 / 4096.0 + 0.5) as u16;
        let v_bat = (adc_v_bat as f32 * 3.3 * 6.6  * 100.0 / 4096.0 + 0.5) as u16;
        let normalized = adc_v2_5.saturating_sub(adc_i_bat * 2);
        let i_bat = ((normalized as f32 * 100.0f32)  / 81.92f32 + 0.5f32) as u16;

        let duty = pwm.get_duty();
        match (adc_v_bat.partial_cmp(&500).unwrap(), duty > 0, duty < (pwm.get_max_duty() - 1))
        {
            (Ordering::Less, _, true) => pwm.set_duty(duty + 1),
            (Ordering::Greater, true, _) => pwm.set_duty(duty - 1),
            _ => (),
        }

        lcd.set_pos(0x5).ok();
        lcd.write(&[b'0' + (v_in / 1000) as u8,
                        b'0' + ((v_in / 100) % 10) as u8,
                        b',',
                        b'0' + (v_in / 10 % 10) as u8,
                        b'0' + (v_in % 10) as u8]).ok();

        // Output Vbat measured value
        lcd.set_pos(0x42).ok();
        lcd.write(&[b'0' + (v_bat / 1000) as u8,
                        b'0' + ((v_bat / 100) % 10) as u8,
                        b',',
                        b'0' + (v_bat / 10 % 10) as u8,
                        b'0' + (v_bat % 10) as u8,]).ok();

        // Output Ibat measured value
        lcd.set_pos(0x4B).ok();
        lcd.write(&[b'0' + (i_bat / 100 % 10) as u8,
                        b',',
                        b'0' + (i_bat / 10 % 10) as u8,
                        b'0' + (i_bat % 10) as u8]).ok();

    }
}
